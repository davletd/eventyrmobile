/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  WebView,
  BackAndroid
} from 'react-native';

const WEBVIEW_REF = "eventenix";

export default class EventyrMobile extends Component {
  constructor(props) {
    super(props);
    this.state = { backButtonEnabled: false };
  }

  componentDidMount() {
      BackAndroid.addEventListener('hardwareBackPress', this.backHandler);
  }

  componentWillUnmount(){
      BackAndroid.removeEventListener('hardwareBackPress', this.backHandler);
  }
    

  render() {
    return (
      <WebView
        ref={WEBVIEW_REF}
        domStorageEnabled={true}
        onNavigationStateChange={this.onNavigationStateChange}
        source={{uri: 'http://eventenix.com/'}}
      />
    );
  }

  backHandler = () => {
    if(this.state.backButtonEnabled) {
        this.refs[WEBVIEW_REF].goBack();
        return true;
    }
  }

  onNavigationStateChange = (navState) => {
    this.setState({
        backButtonEnabled: navState.canGoBack,
    });
  }

}